let form = document.querySelector('form[name="product_form"]');

$(form).on('submit', function (e) {

    e.preventDefault();

    let method = $(form).attr('method');

    if($(form).find('input[name="_method"]').length > 0) {
        method = $(form).find('input[name="_method"]').val();
    }

    $.ajax({
        url: $(form).attr('action'),
        data: new FormData(form),
        type: method,
        contentType: false,
        processData: false,
        cache: false,
        dataType: "json",
        error: function(xhr, status, error) {
            $('.alert.alert-danger').html('').addClass('d-block');
            let data = JSON.parse(xhr.responseText);
            for (let error in data.errors) {
                $('.alert.alert-danger').append('<span class="text-capitalize">[' + error + ']</span> ' + data.errors[error] + '<br/>');
            }
        },
        success: function() {
            window.location.href = "/";
        },
        complete: function() {

        }
    });

});

$('.delete-product').on('click', function (e) {

    if(!confirm("Are you sure delete this product ?")) {
        return false;
    }

    const link = $(this);

    e.preventDefault();

    $.ajax({
        url: link.attr('href'),
        type: 'DELETE',
        dataType: "json",
        error: function(err) {
            console.log(err);
            $('.alert.alert-danger').html()
        },
        success: function() {
            window.location.reload();
        },
        complete: function() {

        }
    });

});

$(document).ready(function () {
    bsCustomFileInput.init()
})