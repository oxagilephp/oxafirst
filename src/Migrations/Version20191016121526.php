<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Creating product table
 */
final class Version20191016121526 extends AbstractMigration
{
    /**
     * Migration description
     * @return string
     */
    public function getDescription(): string
    {
        return 'Product table creation';
    }

    /**
     * Up sql query
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE product (
                     id INT AUTO_INCREMENT NOT NULL,
                     name VARCHAR(100) UNIQUE NOT NULL, 
                     description TEXT DEFAULT NULL,
                     price DOUBLE PRECISION NOT NULL, 
                     count INT NOT NULL,
                     logo VARCHAR(100) NOT NULL,
                     UNIQUE INDEX UNIQ_NAME (name),
                     PRIMARY KEY(id)
                 ) 
                 DEFAULT CHARACTER SET utf8mb4 
                 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB'
        );
    }

    /**
     * Drop created table
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE product');
    }
}
