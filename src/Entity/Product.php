<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Product entity Class.
 *
 * @ORM\Table(name="`product`")
 * @UniqueEntity("name")
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * Id of product table.
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Product name.
     * @ORM\Column(type="text", length=100, unique=true)
     */
    private $name;

    /**
     * Product description.
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * Product price.
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * Product available count.
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * Product logo.
     * @ORM\Column(type="text", length=100)
     */
    private $logo;

    /**
     * Get product's id.
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get product's name.
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set product's name.
     * @param string $name
     * @return Product
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get product's description
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set product's description
     * @param null|string $description
     * @return Product
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get product's price
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * Set product's price
     * @param float $price
     * @return Product
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get product's count
     * @return int|null
     */
    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * Set product's count
     * @param int $count
     * @return Product
     */
    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get product's logo filename
     * @return null|string
     */
    public function getLogo(): ?string
    {
        return $this->logo;
    }

    /**
     * Set product's logo filename
     * @param string $logo
     * @return Product
     */
    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }
}
