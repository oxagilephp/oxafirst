<?php

namespace App\Helpers;

use Symfony\Component\Form\FormInterface;

/**
 * Class FormHelper.
 * @package App\Helpers
 */
class FormHelper
{
    /**
     * Getting all errors from form validation.
     *
     * @param FormInterface $form
     * @return array
     */
    public function getErrorMessages(FormInterface $form)
    {
        $errors = [];

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['common'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }

}