<?php

namespace App\Controller;

use App\Entity\Product;
use App\Forms\ProductFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ProductPageController. Responsible to showing view pages and CRUD.
 *
 * @package App\Controller
 */
class ProductPageController extends AbstractController
{
    /**
     * Homepage action for read all products.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show()
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();

        return $this->render('products/show.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * Add product page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPageAction()
    {
        $form = $this->createForm(ProductFormType::class, null, [
            'action' => $this->generateUrl('api_add_product'),
            'method' => 'POST',
        ]);

        return $this->render('products/add.html.twig', [
            'productForm' => $form->createView()
        ]);
    }

    /**
     * Edit product page
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editPageAction(int $id)
    {
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->find($id);

        if (!$product) {
            throw new NotFoundHttpException('Product not found');
        }

        $form = $this->createForm(ProductFormType::class, null, [
            'action' => $this->generateUrl('api_update_product', ['id' => $id]),
        ]);

        return $this->render('products/edit.html.twig', [
            'productForm' => $form->createView(),
            'product' => $product,
        ]);
    }



}
