<?php

namespace App\Controller\Api;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityNotFoundException;
use Exception;
use App\Entity\Product;
use App\Forms\ProductFormType;
use App\Helpers\FormHelper;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ProductController for REST API.
 * @package App\Controller\Api
 */
class ProductController extends AbstractController
{
    /**
     * Object for manipulation with data
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Object for getting normalized errors as array
     * @var FormHelper
     */
    private $formHelper;

    /**
     * Object for validation fields from entity.
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ProductController constructor.
     * @param EntityManagerInterface $entityManager
     * @param FormHelper $formHelper
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormHelper $formHelper,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->formHelper = $formHelper;
        $this->validator = $validator;
    }

    /**
     * Add new product.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm(ProductFormType::class);

        $form->handleRequest($request);

        $response = [];
        $status = Response::HTTP_CREATED;

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $product = new Product();
            $product->setName($data['name']);
            $product->setDescription($data['description']);
            $product->setPrice($data['price']);
            $product->setCount($data['count']);

            $errors = $this->validator->validate($product);
            foreach ($errors as $error) {
                $form->get($error->getPropertyPath())->addError(new FormError($error->getMessage()));
            }

            if ($errors->count() === 0) {
                $uploader = new FileUploader('./storage');

                try {
                    $fileName = $uploader->upload($data['logo']);
                    $product->setLogo($fileName);

                    $this->entityManager->persist($product);
                    $this->entityManager->flush();

                    $this->addFlash('success', 'Product created!');
                } catch (UploadException $e) {
                    $form->get('logo')->addError(new FormError($e->getMessage()));
                } catch (DBALException $e) {
                    $response = ['errors' => ['common' => 'DB handle error']];
                } catch (Exception $e) {
                    $response = ['errors' => ['common' => $e->getMessage()]];
                }
            }
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $response['errors'] = $this->formHelper->getErrorMessages($form);
            $status = Response::HTTP_BAD_REQUEST;
        }

        return $this->json($response, $status);
    }

    /**
     * Update product with by id
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function updateAction(Request $request, int $id)
    {
        $form = $this->createForm(ProductFormType::class);

        $form->handleRequest($request);

        $response = [];
        $status = Response::HTTP_OK;

        if ($form->isValid()) {
            $data = $form->getData();

            $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
            $product->setName($data['name']);
            $product->setDescription($data['description']);
            $product->setPrice($data['price']);
            $product->setCount($data['count']);

            $errors = $this->validator->validate($product);
            foreach ($errors as $error) {
                $form->get($error->getPropertyPath())->addError(new FormError($error->getMessage()));
            }

            if ($data['logo'] instanceof UploadedFile) {
                $uploader = new FileUploader('./storage');

                try {
                    $fileName = $uploader->upload($data['logo']);
                    $product->setLogo($fileName);
                } catch (UploadException $e) {
                    $form->get('logo')->addError(new FormError($e->getMessage()));
                } catch (Exception $e) {
                    $response = ['errors' => ['common' => $e->getMessage()]];
                }
            }

            if ($errors->count() === 0) {
                try {
                    $this->entityManager->persist($product);
                    $this->entityManager->flush();

                    $this->addFlash('success', 'Product updated!');
                } catch (EntityNotFoundException $e) {
                    $response['errors'] = ['common' => 'Product with this id not found'];
                    $status = Response::HTTP_NOT_FOUND;
                } catch (DBALException $e) {
                    $response = ['errors' => ['common' => 'DB handle error']];
                } catch (Exception $e) {
                    $response = ['errors' => ['common' => $e->getMessage()]];
                }
            }
        }

        if (!$form->isValid()) {
            $response['errors'] = $this->formHelper->getErrorMessages($form);
            $status = Response::HTTP_BAD_REQUEST;
        }

        return $this->json($response, $status);
    }

    /**
     * Delete product by id
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteAction(int $id)
    {
        $response = [];
        $status = Response::HTTP_OK;

        try {
            $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
            $this->entityManager->remove($product);
            $this->entityManager->flush();
            $this->addFlash('success', 'Product deleted!');
        } catch (EntityNotFoundException $e) {
            $response['errors'] = ['common' => 'Product with this id not found'];
            $status = Response::HTTP_NOT_FOUND;
        } catch (Exception $e) {
            $response['errors'] = ['common' => 'Something went wrong!'];
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return $this->json($response, $status);
    }

}
